#pragma once

void spin_globe();
void init_globe(Window *window);
void destroy_globe();
void set_sun_position(uint16_t longitude, uint16_t latitude);